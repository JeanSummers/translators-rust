mod process;
mod parser;
mod check;
mod optimizer;

mod error;

mod token;
mod node;

mod interactive;
mod fileread;

pub use interactive::start as interactive;
pub use fileread::start as fileread;