use super::node::{ Node, Node::* };

pub fn run(node: &mut Box<Node>) {
    fold_const(node);
    fold_negation(node);
    fold_same(node);
}

/// ```
/// x | 0 = x    
/// x | 1 = 1    
/// x | x = x    
/// x | ~x = 1    
///     
/// x & 0 = 0    
/// x & 1 = x    
/// x & x = x    
/// x & ~x = 0   
/// ``` 
fn fold_same(node: &mut Box<Node>) {
    match &mut **node {
        Operator{id, left, right} => {
            fold_same(left);
            fold_same(right);

            macro_rules! fold_negation {
                ($a:ident, $b:ident) => {
                    match &mut **$a {
                        Function{id: b'~', arg} if arg == $b => match *id {
                            b'&' => return *node = make_zero(),
                            b'|' => return *node = make_one(),
                            _ => ()
                        },
                        _ => ()
                    };
                }
            }

            fold_negation!(left, right);
            fold_negation!(right, left);

            match *id {
                b'&' | b'|' if left == right => *node = move_node(left),

                b'&' if is_one(left) => *node = move_node(right),
                b'&' if is_one(right) => *node = move_node(left),
                
                b'&' if is_zero(left) || is_zero(right) => *node = make_zero(),
                
                b'|' if is_one(left) || is_one(right) => *node = make_one(),

                b'|' if is_zero(left) => *node = move_node(right),
                b'|' if is_zero(right) => *node = move_node(left),
                _ => (),
            }
        },
        Function{arg, ..} => fold_same(arg),
        _ => (),
    }
}


fn make_zero() -> Box<Node> {
    Node::number(0)
}

fn is_zero(node: &mut Box<Node>) -> bool {
    if let Number{value: 0 } = &**node { true } else { false }
}

const BITWISE_ONE: u32 = !0u32;

fn make_one() -> Box<Node> {
    Node::number(BITWISE_ONE)
}

fn is_one(node: &mut Box<Node>) -> bool {
    if let Number{value: BITWISE_ONE } = &**node { true } else { false }
}

fn fold_negation(node: &mut Box<Node>) {
    match &mut **node {
        Operator{id, left, right} => {
            fold_negation(left);
            fold_negation(right);

            if let Function{id: b'~', arg: left_arg} = &mut **left {
                if let Function{id: b'~', arg: right_arg} = &mut **right {
                    let inverse_sign = match *id {
                        b'&' => b'|',
                        b'|' => b'&',
                        _ => return,
                    };
                    *node = Node::function(b'~',
                        Node::operator(inverse_sign, move_node(left_arg), move_node(right_arg)));
                }
            }
        },
        Function{id: b'~', arg} => {
            fold_negation(arg);
            if let Function{id: b'~', arg: inner_arg} = &mut **arg {
                *node = move_node(inner_arg);
            }
        },
        Function{arg, ..} => fold_negation(arg),
        _ => (),
    }
}

/// Moved child nodes need to be replaced by blank value
/// in order to not leave invalid boxes in syntax tree
/// 
/// As an alternative to pointless allocations before every
/// upward node movement, Rc can be used (with additional runtime cost)
fn move_node(node: &mut Box<Node>) -> Box<Node> {
    std::mem::replace(node, Node::number(0))
}

fn fold_const(node: &mut Box<Node>) {
    match &mut **node {
        Function{id, arg} => {
            fold_const(arg);

            if let Number{value} = **arg {
                if *id == b'~' {
                    *node = Node::number(!value)
                }
            }
        },
        Operator{id, left, right} => {
            fold_const(left);
            fold_const(right);

            if let Number{value: left} = **left {
                if let Number{value: right} = **right {
                    match *id {
                        b'&' => *node = Node::number(left & right),
                        b'|' => *node = Node::number(left | right),
                        _ => (),
                    }
                }
            }
        },
        _ => ()
    }
}


/*
Могло бы использоваться для реализации линейных оптимизаций,
но на этот проект и так потрачено слишком много времени. 

В теории можно было бы разработать структуру для хранения линейных токенов. 
enum LinearToken { Num(u32), Var(Node), Neg(Node) }

И реализовать рекурсивную функцию, эти токены собирающую
fn collect_linear(node: Node) -> { tokens: Vec<LinearToken>, operator: u8 }

А так же функцию для сортировки массива токенов и выполнения линейных оптимизаций

Плюсы:
Можно линейно сворачивать константы, применять оптимизации по типу fold_same
Но основное преимущество линейных оптимизаций в сворачивании выражений навроде a & c & ~a = 0
(Поскольку токены сортируются таким образом a & ~a & c и свертка становится тривиальной)

Алгоритм функции fn linear_optimize_[or|and](tokens: Vec<LinearToken>) -> Node
Входной массив сортируется
Проходим по списку токенов, сравниваем пары и складываем результат сравнения в стек
Если длина стека меньше длины входного массива, 
    то они меняются местами, стек очищается, после чего сортировка и оптимизация повторяются.
Если длина стека равна длине входного массива,
    то стек последовательно преобразуется обратно в синтаксическое дерево

*/
#[allow(dead_code)]
fn propagate_negation(node: &Box<Node>, negative: bool) -> Box<Node> {
    match &**node {
        Operator{id, left, right} => if negative {
            Node::operator(
                if *id == b'&' { b'|' } else { b'&' }, 
                propagate_negation(&left, true), 
                propagate_negation(&right, true)
            )
        } else {
            Node::operator(
                *id, 
                propagate_negation(&left, false), 
                propagate_negation(&right, false)
            )
        },
        Function{arg, ..} => if negative {
            propagate_negation(&arg, false)
        } else {
            propagate_negation(&arg, true)
        },
        Variable{id} => if negative {
            Node::function(b'~', Node::variable(*id))
        } else {
            Node::variable(*id)
        },
        Number{value} => if negative {
            Node::number(!value)
        } else  {
            Node::number(*value)
        }
    }
}