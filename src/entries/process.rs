use super::error::GrammarError;
use crate::try_partial;
use super::parser;
use super::check;
use super::optimizer;

pub fn run(input: &str) -> Result<String, GrammarError> {
    check::unsupported_chars(input)?;

    let tokens = parser::str_to_tokens(input)?;

    try_partial!(check::following_rules(&tokens), input);
    try_partial!(check::brackets(&tokens), input);

    let postfix = parser::tokens_to_postfix(tokens)?;

    let mut node = parser::postfix_to_tree(postfix)?;

    optimizer::run(&mut node);

    Ok(node.to_string())
}

