#[derive(Debug)]
pub enum Token {
    OpeningBracket,
    ClosingBracket,
    Number(u32),
    Variable(char),
    Operator(char),
    Function(char),
}

use Token::*;

impl Token {
    pub fn id(&self) -> String {
        match self {
            OpeningBracket => '('.to_string(),
            ClosingBracket => ')'.to_string(),
            Number(value) => format!("{:b}", value),
            Variable(id) | Operator(id) | Function(id) => id.to_string(),
        }
    }
}