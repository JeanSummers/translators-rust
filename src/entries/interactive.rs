use std::io;
use std::io::Write;

use super::process;

pub fn start() {
    println!("Введите выражение");
    loop {
        let mut line = String::new();
        get_input(&mut line).unwrap();

        match line.trim() {
            "q" | "quit" => return,
            "" => continue,
            _ => ()
        };

        match process::run(line.trim_end()) {
            Ok(result) => println!("{}", result),
            Err(err) => eprintln!("{}", err),
        }
    }
}

fn get_input(buffer: &mut String) -> io::Result<usize> {
    io::stdout().write(b"> ")?;
    io::stdout().flush()?;
    io::stdin().read_line(buffer)
}