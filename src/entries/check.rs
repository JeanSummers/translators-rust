use super::error::{ GrammarError, PartialError };
use super::token::{ Token, Token::* };

pub fn unsupported_chars(input: &str) -> Result<(), GrammarError> {
    for (i, item) in input.chars().enumerate() {
        match item {
            ' ' | '(' | ')' | 
            '~' | '&' | '|' | 
            '0'..='1' | 'a'..='z' | 'A'..='Z' => continue,
            _ => return Err(GrammarError::positional(
                &format!("Символа '{}' нет в словаре", item), input, i
            )),
        }
    }
    Ok(())
}

pub fn brackets(tokens: &[(Token, usize)]) -> Result<(), PartialError> {
    let mut stack = vec![];
    for (token, position) in tokens.iter() {
        match token {
            OpeningBracket => stack.push(position),
            ClosingBracket if stack.len() == 0 => 
                return Err(PartialError::new("Лишняя закрывающая скобка", *position)),
            ClosingBracket => { stack.pop(); },
            _ => continue
        }
    }

    if stack.len() > 0 {
        Err(PartialError::new("Незакрытая скобка", **(stack.last().unwrap())))
    } else {
        Ok(())
    }
}

pub fn following_rules(tokens: &[(Token, usize)]) -> Result<(), PartialError> {
    if tokens.len() == 0 {
        return Ok(());
    }

    let first_token = &tokens[0].0;
    match first_token {
        OpeningBracket | Function(_) | Variable(_) | Number(_) => (),
        _ => return Err(PartialError::new(
            &format!("'{}' не может находиться в начале строки", first_token.id()), tokens[0].1
        ))
    }

    let mut last_token = first_token;
    for (current_token, current_position) in tokens[1..].iter() {
        if !check_rule(&last_token, &current_token) {
            return Err(PartialError::new(
                &format!("После '{}' не может следовать '{}'", last_token.id(), current_token.id()), *current_position
            ))
        }
        last_token = current_token;
    }

    match last_token {
        ClosingBracket | Variable(_) | Number(_) => (),
        _ => return Err(PartialError::new(
            &format!("'{}' не может находиться в конце строки", last_token.id()), tokens.last().unwrap().1
        ))
    }

    Ok(())
}

fn check_rule(first: &Token, second: &Token) -> bool {
    match first {
        OpeningBracket => match second {
            ClosingBracket | OpeningBracket | Function(_) | Variable(_) | Number(_) => true,
            _ => false
        },
        Operator(_) | Function(_) => match second {
            OpeningBracket | Function(_) | Variable(_) | Number(_) => true,
            _ => false
        },
        ClosingBracket | Variable(_) | Number(_) => match second {
            ClosingBracket | Operator(_)  => true,
            _ => false
        },
    }
}