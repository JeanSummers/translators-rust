use std::fmt::{ self, Display };

#[derive(Debug)]
pub enum Node {
    Operator {
        id: u8,
        left: Box<Node>,
        right: Box<Node>,
    },
    Function {
        id: u8,
        arg: Box<Node>,
    },
    Variable {
        id: u8,
    },
    Number {
        value: u32,
    }
}

use Node::*;

impl Node {
    pub fn number(value: u32) -> Box<Node> {
        Box::new(Number { value })
    }
    
    pub fn variable(id: u8) -> Box<Node> {
        Box::new(Variable { id })
    }
    
    pub fn function(id: u8, arg: Box<Node>) -> Box<Node> {
        Box::new(Function { id, arg })
    }

    pub fn operator(id: u8, left: Box<Node>, right: Box<Node>) -> Box<Node> {
        Box::new(Operator { id, left, right })
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Operator{id, left, right} => 
                if let Operator{id: other_id, left: other_left, right: other_right} = other {
                    id == other_id && left == other_left && right == other_right
                } else { false },
            Function{id, arg} => 
                if let Function{id: other_id, arg: other_arg} = other {
                    id == other_id && arg == other_arg
                } else { false },
            Variable{id} => 
                if let Variable{id: other_id} = other {
                    id == other_id
                } else { false },
            Number{value} => 
                if let Number{value: other_value} = other {
                    value == other_value
                } else { false }
        }
    }
}
impl Eq for Node {}

impl Display for Node {
    #[cfg(feature = "postfix")]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Operator{id, left, right} => 
                write!(f, "{} {} {}", left, right, *id as char),
            Function{id, arg} => 
                write!(f, "{} {}", arg, *id as char),
            Variable{id} => 
                write!(f, "{}", *id as char),
            Number{value} =>
                write!(f, "{:b}", value),
        }
    }
    
    #[cfg(feature = "prefix")]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Operator{id, left, right} => 
                write!(f, "{} {} {}", *id as char, left, right),
            Function{id, arg} => 
                write!(f, "{} {}", *id as char, arg),
            Variable{id} => 
                write!(f, "{}", *id as char),
            Number{value} =>
                write!(f, "{:b}", value),
        }
    }

    #[cfg(feature = "infix")]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Operator{id, left, right} => {
                let left = enclose_operand(*id, left);
                let right = enclose_operand(*id, right);

                write!(f, "{} {} {}", left, *id as char, right)
            },
            Function{id, arg} => {
                let arg = enclose_argument(arg);
                write!(f, "{}{}", *id as char, arg)
            },
            Variable{id} => 
                write!(f, "{}", *id as char),
            Number{value} =>
                write!(f, "{:b}", value),
        }
    }
}

#[cfg(feature = "infix")]
fn enclose_argument(arg: &Box<Node>) -> String {
    match **arg {
        Operator{..} => format!("({})", arg),
        _ => arg.to_string(),
    }
}

#[cfg(feature = "infix")]
fn enclose_operand(parent_id: u8, operand: &Box<Node>) -> String {
    use super::parser::priority;

    match **operand {
        Operator{id: operand_id, ..} 
            if priority(parent_id as char) > priority(operand_id as char) => 
                format!("({})", operand),
        _ => operand.to_string(),
    }
}