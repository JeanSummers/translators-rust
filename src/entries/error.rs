use std::error::Error;
use std::fmt::{ self, Display, Write };

#[derive(Debug)]
pub enum GrammarError {
    Single(String),
    Positional {
        message: String,
        original: String,
        position: usize,
    },
}

use GrammarError::*;

impl GrammarError {
    pub fn new(message: &str) -> GrammarError {
        Single(message.to_string())
    }

    pub fn positional(message: &str, original: &str, position: usize) -> GrammarError {
        Positional {
            message: message.to_string(),
            original: original.to_string(),
            position,
        }
    }
}

impl Display for GrammarError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Single(msg) => f.write_str(msg),
            Positional{ message, original, position } => {
                f.write_str(&format!("# {}\n", &original))?;
                f.write_str("# ")?;
                for _ in 0..*position {
                    f.write_char(' ')?;
                }
                f.write_str("^\n")?;
                f.write_str(message)
            }
        }
    }
}

impl Error for GrammarError {}

#[derive(Debug)]
pub struct PartialError {
    message: String,
    position: usize,
}

impl PartialError {
    pub fn new(message: &str, position: usize) -> PartialError {
        PartialError {
            message: message.to_string(),
            position
        }
    }

    pub fn to_full(&self, original: &str) -> GrammarError {
        GrammarError::positional(&self.message, original, self.position)
    }
}

impl Display for PartialError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.write_str(&self.message)?;
        f.write_str(&format!(" на позиции {}", self.position))
    }
}


impl Error for PartialError {}

#[macro_export]
macro_rules! try_partial {
    ($exp:expr, $complement:expr) => {
        match $exp {
            Ok(result) => result,
            Err(err) => return Err(err.to_full($complement))
        }
    };
}