use std::fs;

use super::process;

pub fn start(filename: &str) {
    println!("Run fileread mode on file {}", filename);

    let contents = match fs::read_to_string(filename) {
        Ok(result) => result,
        Err(_) => return eprintln!("Не удалось прочитать файл {}", filename)
    };

    for (i, line) in contents.lines().enumerate() {
        if !line.trim().is_empty() {
            match process::run(line) {
                Ok(result) => {
                    println!("I: {}", line);
                    println!("O: {}\n", result);
                },
                Err(err) => {
                    eprintln!("\nОшибка в строке {}", i + 1);
                    eprintln!("{}\n", err);
                }
            }
        }
    }
}