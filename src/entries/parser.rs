use super::error::GrammarError;
use super::token::Token;
use super::node::Node;

pub fn str_to_tokens(input: &str) -> Result<Vec<(Token, usize)>, GrammarError> {
    use Token::*;

    let bytes = input.as_bytes();

    let mut result = vec![];
    let mut position = 0;
    while position < bytes.len(){
        let mut offset = 1;
        let token = match bytes[position] {
            b'(' => OpeningBracket,
            b')' => ClosingBracket,
            b'~' => Function('~'),
            b'&' | b'|' => Operator(bytes[position] as char),
            b'a'..=b'z' | b'A'..=b'Z' => Variable(bytes[position] as char),
            b'0'..=b'1' => {
                let (value, end) = parse_number(bytes, input, position)?;
                offset = end - position;
                Number(value)
            },
            _ => { 
                position += 1;
                continue;
            }
        };
        result.push((token, position));
        position += offset;
    }
    Ok(result)
}

fn parse_number(bytes: &[u8], input: &str, start: usize) -> Result<(u32, usize), GrammarError> {
    let mut end = start + 1;
    while end < bytes.len() {
        match bytes[end] {
            b'0'..=b'1' => end += 1,
            _ => break
        }
    }
    if end - start > 32 {
        return Err(GrammarError::positional(
            "Слишком большое число", input, start))
    }

    // No-overhead cast from &[u8] to &str
    let str_slice = unsafe { std::str::from_utf8_unchecked(&bytes[start..end]) };

    match u32::from_str_radix(str_slice, 2) {
        Ok(value) => Ok((value, end)),
        Err(_) => Err(GrammarError::positional(
            "Ошибка при распознавании числа", input, start))
    }
}

pub fn postfix_to_tree(tokens: Vec<Token>) -> Result<Box<Node>, GrammarError> {
    let mut stack = vec![];

    macro_rules! pop_stack {
        ($id:ident) => {
            match stack.pop() {
                Some(value) => value,
                None => return Err(GrammarError::new(&format!("Не достаточно аргументов для оператра {}", $id)))
            }
        };
    }

    for token in tokens.iter() {
        let node = match token {
            Token::Number(value) => Node::number(*value),
            Token::Variable(id) => Node::variable(*id as u8),
            Token::Function(id) => {
                let arg = pop_stack!(id);
                Node::function(*id as u8, arg)            
            },
            Token::Operator(id) => {
                let right = pop_stack!(id);
                let left = pop_stack!(id);
                Node::operator(*id as u8, left, right)  
            },
            _ => return Err(GrammarError::new(
                "Неправильные входные данные для синтаксического дерева"))
        };
        stack.push(node);
    };

    match stack.pop() {
        Some(value) => Ok(value),
        None => return Err(GrammarError::new(
            "Не получилось составить синтаксическое дерево. Возможно выражение неправильное"))
    }
}

pub fn tokens_to_postfix(pairs: Vec<(Token, usize)>) -> Result<Vec<Token>, GrammarError> {
    use Token::*;

    let mut result = vec![];
    let mut stack = vec![];

    for (token, _) in pairs.into_iter() {
        match token {
            Number(_) | Variable(_) => result.push(token),
            Function(_) => stack.push(token),
            OpeningBracket => stack.push(token),
            ClosingBracket => loop {
                let top = match stack.pop() {
                    Some(value) => value,
                    None => return Err(GrammarError::new("Выражение содержит не комплементарные скобки"))
                };
                match top {
                    OpeningBracket => break,
                    _ => result.push(top)
                }
            },
            Operator(id) => {
                loop {
                    let top = match stack.pop() {
                        Some(value) => value,
                        None => break
                    };
                    match top {
                        Function(_) => result.push(top),
                        Operator(top_id) if priority(top_id) >= priority(id) => result.push(top),
                        _ => {
                            stack.push(top);
                            break;
                        }
                    }
                }
                stack.push(token)
            } 
        }
    }

    while let Some(token) = stack.pop() {
        result.push(token)
    }

    Ok(result)
}

pub fn priority(operator: char) -> u32 {
    match operator {
        '&' => 2,
        '|' => 1,
        _ => unreachable!()
    }
}