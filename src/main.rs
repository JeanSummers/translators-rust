mod entries;

use std::env;

fn main() {
    let mut args = env::args().skip(1);
    match args.next() {
        Some(filename) => entries::fileread(&filename),
        None => entries::interactive()
    }
}
